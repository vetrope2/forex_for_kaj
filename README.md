# FOREX trading pro KAJ
## Cíl projektu
Cílem je webová aplikace, která slouží pro automatizaci lidského rozhodování na trhu s cizími měnami, konkrétně parita Eura a Amerického dolaru. Aplikace dává člověku odpovědi zda se má Prodávat či Nakupovat. Nabízí tři strategie pohledu na devizový trh a uživatel si může zvolit jakoukoliv z nich, tu požívat jak je nebo si měnit jejich parametry dle vlastního uvážení.

## Stručný popis stránek
Na hlavní straně se nachází graf vývoje ceny za posledních 20 dní. Vpravo od něj je políčko, na kterém se ukazuje vývoj ceny za poslední dva dny a doporučení každé ze strategií pro dnešní den.

Na stránce druhé je k dispozici simulace, kde si uživatel může zvolit jednu ze strategií a časové období pro které bude simulace spuštěna. Vpravo se ukáží výsledky simulace s daty, cenami a informací zda kupovat či prodávat.

Na poslední stránce je nastavení možných parametrů, které se uloží do local storage.

## Třídy
**DataEntry** - obaluje datum a cenu v ten den

**Data** - Zaobaluje seznam složený z DataEntry a metody, které souvisejí s hledáním v daném seznamu a některých statistických výpočtů nad nimi

**Strategy** - obsahuje 3 algoritmy pro cenovou predikci

**DailyPage, SimulationPage a SettingsPage** - odpovídají stránkám aplikace

## Algoritmy
**Bollingerova Pásma**

**Návrat k průměru**

**Aroon indikátor**

## Hodnocení tabulky:

### HTML5

**Validita** - zkontrolována

**Sémantické značky** - použito nav, main, section

**Formulářové prvky** - pro nastavení číselných hodnot dovoluje input pouze čísla

**Offline aplikace** - pokud se nelze připojit k internetu a aplikace si nestihla stáhnout data z 	API, tak vypíše chybovou hlášku ( děláno v JS přes try catch)

-žádnou offline funkcionalitu to nenabízí, protože vše závisí na API s daty

### CSS

**Pokročilé selektory** - použita pseudotřída hover, hierarchie, id a třídy

**CSS transformace**- při najetí myší na menu se tlačítko zvětší

**CSS transitions** - předchozí transformace se děje pomocí transition: 1s

**Media queries** - Při přílišném zmenšení stránky se obsah upraví do sloupce, zmizí nápis 		menu( už by tam překážel) a položky menu se vyskládají do řádku 	nahoře na stránce

### Javascript
**OOP** - aplikace je strukturována do tříd -> DataEntry, Data, Strategy a jednotlivé Pages

**Knihovny** - z JQuery je použito kalendářový input

**Pokročilá API** - do localStorage se ukládají Vaše nastavení používaných algoritmů

**Offline aplikace** - jen skrz obyčejný try catch a HttpRequest

**Bonus** - Stránky se přepínají podobně jako Router na cvičení


